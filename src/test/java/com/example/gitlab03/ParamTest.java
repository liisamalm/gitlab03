package com.example.gitlab03;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.Matchers.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ParamTest {

    @Autowired
    private MockMvc mockMvc;

    @org.junit.jupiter.api.Test
    public void notEmptyName() throws Exception {
        this.mockMvc.perform(get("/hello?etunimi=etunimi&sukunimi=sukunimi"))
        .andExpect(content().string(containsString("etunimi")))
                .andExpect(content().string("etunimi"));
                

    }
    
}
