package com.example.gitlab03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gitlab03Application {

	public static void main(String[] args) {
		SpringApplication.run(Gitlab03Application.class, args);
	}

}
