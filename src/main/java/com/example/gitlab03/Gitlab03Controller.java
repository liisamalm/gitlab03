package com.example.gitlab03;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Gitlab03Controller {

    private WholeName name = new WholeName();

    @GetMapping("/index")
        public String home() {
        return "index";
    }

    @GetMapping("/hello")
    public String showName(Model model) {
        model.addAttribute("nimi", name);
        return "hello";
    }
    
    @PostMapping("/index")
        public String addName(@RequestParam String etunimi, @RequestParam String sukunimi) {
            if (etunimi.isEmpty() || sukunimi.isEmpty()) {  
            }
            name.setEtunimi(etunimi);
            name.setSukunimi(sukunimi);

            return "redirect:/hello"; 
        }


}
