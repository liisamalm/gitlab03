package com.example.gitlab03;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WholeName {

    private String etunimi;
    private String sukunimi;
    
}
